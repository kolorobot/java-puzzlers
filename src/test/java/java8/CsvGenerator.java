package java8;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CsvGenerator {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("src/test/resources", "large-sample.csv");
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, Charset.forName("UTF-8"));) {
            for (int i = 0; i < 10000000; i++) {
                bufferedWriter.write("record-" + Double.valueOf(Math.random()* i).intValue() + ";" + i);
                bufferedWriter.newLine();
            }
        }
    }
}
