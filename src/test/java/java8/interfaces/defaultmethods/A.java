package java8.interfaces.defaultmethods;

@FunctionalInterface
public interface A {

    void action();

    default String getName() {
        return this.getClass().getName();
    }
}

interface B extends A {

    // redefine the default method of A, which overrides the original method
    @Override
    default String getName() {
        return this.getClass().getName();
    }
}

interface C extends B {
    // redeclare the default method, which makes it abstract
    @Override
    String getName();
}

interface D extends C {
    // not mention the default method at all, which lets your extended interface inherit the default method.
}

class ClazzA implements A {

    @Override
    public void action() {
    }

}

abstract class AbstractClazzA implements A {

    @Override
    public void action() {
    }

    @Override
    public abstract String getName();
}


class ClazzB implements B {

    @Override
    public void action() {
    }

    // redefine the default method of A, which overrides the original method
    @Override
    public String getName() {
        return "I am ClazzA";
    }
}

class ClazzC implements C {

    @Override
    public void action() {

    }

    @Override
    public String getName() {
        return "I am ClazzC";
    }
}

class ClazzD implements D {


    @Override
    public void action() {

    }

    @Override
    public String getName() {
        return "I am ClazzD";
    }
}



