package java8.interfaces.defaultmethods;

@FunctionalInterface
public interface Printer {

    void print(Object value);

    default Printer thenPrinting(Printer other) {
        return value -> {
            this.print(value);
            other.print(value);
        };
    }
}
