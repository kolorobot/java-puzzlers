package java8.interfaces.defaultmethods;

import org.junit.jupiter.api.Test;

class DefaultMethodsTest {

    @Test
    void test() {

        A a = new ClazzA();
        System.out.println(a.getName());

        B b = new ClazzB();
        System.out.println(b.getName());

        C c = new ClazzC();
        System.out.println(c.getName());

        D d = new ClazzD();
        System.out.println(d.getName());

        A a2 = () -> {};
        System.out.println(a2.getName());

        B b2 = () -> {};
        System.out.println(b2.getName());
    }

}