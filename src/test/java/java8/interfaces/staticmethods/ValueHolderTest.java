package java8.interfaces.staticmethods;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ValueHolderTest {
    @Test
    void holdsTheValue() {
        ValueHolder<Integer> valueHolder = ValueHolder.of(Integer.valueOf(100));
        assertThat(valueHolder.getValue()).isEqualTo(100);
    }
}