package java8.interfaces.staticmethods;

@FunctionalInterface
public interface Printer {

    void print(String value);

    default Printer thenPrinting(Printer other) {
        return value -> {
            this.print(value);
            other.print(value);
        };
    }

    static Printer nullPrinter() {
        return value -> {};
    }

    static Printer consolePrinter() {
        return System.out::println;
    }
}
