package java8.interfaces.staticmethods;

@FunctionalInterface
public interface ValueHolder<T> {

    T getValue();

    static <T> ValueHolder of(T value) {
        return () -> value;
    }
}