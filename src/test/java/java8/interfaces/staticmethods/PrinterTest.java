package java8.interfaces.staticmethods;

import org.junit.jupiter.api.Test;

class PrinterTest {

    @Test
    void test() {
        Printer printer = System.out::println;
        printer
                .thenPrinting(System.err::println)
                .print("Chained printing ...");
    }
}