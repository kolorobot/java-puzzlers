package java8.samples.csv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.stream.Collectors;

class CsvReaderImpl implements CsvReader {

    private static final String SEPARATOR = ";";

    @Override
    public <R> List<R> parse(Reader source, RowMapper<R> rowMapper, String separator, int skipLines) {
        try (BufferedReader reader = new BufferedReader(source)) {
            return reader.lines()
                    .skip(1)
                    .map(line -> line.split(SEPARATOR))
                    .map(rowMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
