package java8.samples.csv;

import java.time.LocalDate;

public class IncidentMapper implements CsvReader.RowMapper<Incident> {
    @Override
    public Incident apply(String[] input) {
        return new IncidentBuilder()
                .withId(Long.valueOf(input[0]))
                .withCreatorId(input[1])
                .withAssigneeId(input[2])
                .createdAt(LocalDate.parse(input[3]))
                .withStatus(Incident.Status.valueOf(input[4]))
                .withSummary(input[5])
                .build();
    }
}
