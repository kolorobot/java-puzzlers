package java8.samples.csv;

import java.time.LocalDate;

public class Incident {

    public enum Status {
        NEW, CONFIRMED, NOT_CONFIRMED, SOLVED, CLOSED;

        public static Status valueOf(int ordinal) {
            for (Status status : Status.values()) {
                if (status.ordinal() == ordinal) {
                    return status;
                }
            }
            return null;
        }
    }

    private final Long id;

    private final String creatorId;

    private final String assigneeId;

    private final LocalDate created;

    private final Status status;

    private final String summary;

    Incident(Long id, String creatorId, String assigneeId, LocalDate created, Status status, String summary) {
        this.id = id;
        this.creatorId = creatorId;
        this.assigneeId = assigneeId;
        this.created = created;
        this.status = status;
        this.summary = summary;
    }

    public Long getId() {
        return id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public String getSummary() {
        return summary;
    }

    public Status getStatus() {
        return status;
    }

    public String getAssigneeId() {
        return assigneeId;
    }

    public LocalDate getCreated() {
        return created;
    }
}
