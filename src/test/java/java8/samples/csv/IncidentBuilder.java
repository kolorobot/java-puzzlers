package java8.samples.csv;

import java.time.LocalDate;

public class IncidentBuilder {

    private Long id;
    private String creatorId;
    private String assigneeId;
    private LocalDate created;
    private Incident.Status status;
    private String summary;

    public IncidentBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public IncidentBuilder withCreatorId(String creatorId) {
        this.creatorId = creatorId;
        return this;
    }

    public IncidentBuilder withAssigneeId(String assigneeId) {
        this.assigneeId = assigneeId;
        return this;
    }

    public IncidentBuilder createdAt(LocalDate created) {
        this.created = created;
        return this;
    }

    public IncidentBuilder withStatus(Incident.Status status) {
        this.status = status;
        return this;
    }

    public IncidentBuilder withSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public Incident build() {
        return new Incident(id, creatorId, assigneeId, created, status, summary);
    }
}