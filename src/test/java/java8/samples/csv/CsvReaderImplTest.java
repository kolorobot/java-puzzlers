package java8.samples.csv;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CsvReaderImplTest {

    @Test
    void readsNotEmptyFile() throws IOException {
        CsvReader csvReader = new CsvReaderImpl();
        Path path = Paths.get("src/test/resources/csv", "incidents.csv");
        Reader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);

        List<Incident> incidents = csvReader.parse(reader, new IncidentMapper());
        assertThat(incidents).isNotNull();
    }
}