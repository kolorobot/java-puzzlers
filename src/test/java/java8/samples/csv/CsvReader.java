package java8.samples.csv;

import java.io.Reader;
import java.util.List;
import java.util.function.Function;

public interface CsvReader {

    String DEFAULT_SEPARATOR = ";";

    @FunctionalInterface
    interface RowMapper<R> extends Function<String[], R> {
        @Override
        R apply(String[] input);
    }

    default <R> List<R> parse(Reader source, RowMapper<R> rowMapper) {
        return parse(source, rowMapper, DEFAULT_SEPARATOR, 0);
    }

    default <R> List<R> parse(Reader source, RowMapper<R> rowMapper, String separator) {
        return parse(source, rowMapper, separator, 0);
    }

    <R> List<R> parse(Reader source, RowMapper<R> rowMapper, String separator, int skipLines);
}