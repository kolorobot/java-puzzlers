package java8.samples.csv;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IncidentRepository {

    private final List<Incident> incidents;

    public IncidentRepository(List<Incident> incidents) {
        this.incidents = incidents;
    }

    private Stream<Incident> stream() {
        return incidents.stream();
    }

    public List<Incident> filterByAssignee(String assignee) {
        return stream()
                .filter(i -> assignee.equals(i.getAssigneeId()))
                .collect(Collectors.toList());
    }

    public Map<Incident.Status, List<Incident>> groupByStatus() {
        return stream()
                .collect(Collectors.groupingBy(Incident::getStatus));
    }

}
