package java8.samples.zip;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipFile;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.codeleak.samples.puzzlers.core.io.ZipFileBrowser2.*;

class ZipFileBrowser2Test {

    private Path path = Paths.get("src/test/resources/zip", "sample-1.zip");
    private ZipFile zipFile;

    @BeforeEach
    void setUp() throws Exception {
        zipFile = new ZipFile(path.toFile());
    }

    @Test
    void findsTextFilesContainingText() {
        List<String> matchingFiles = getMatchingEntries(zipFile,
                isFile().and(nameMatches(".*txt")),
                withContent("Line 1"));

        assertThat(matchingFiles)
                .containsSequence("1.txt", "1/1-1.txt");
    }

    @Test
    void findsCsvFilesContainingText() {

        List<String> matchingFiles = getMatchingEntries(zipFile,
                isFile().and(nameMatches(".*csv")),
                withContent("22"));

        assertThat(matchingFiles)
                .containsSequence("1/1-2/1-1-2.csv");
    }

    @Test
    void findsDirectoriesMatchingName() {

        List<String> matchingFiles = getMatchingEntries(zipFile,
                isFile().negate().and(nameContains("1")));

        assertThat(matchingFiles)
                .containsSequence("1/", "1/1-2/");
    }


    @Test
    void findsAllFiles() {

        List<String> matchingFiles = getMatchingEntries(zipFile,
                isFile());

        assertThat(matchingFiles)
                .containsSequence("1.txt", "1/1-1.txt", "1/1-2.csv", "1/1-2/1-1-2.csv", "1/1-2/1-1-2.txt", "2.csv", "2/2-1.txt", "2/2-2.csv");
    }

    @Test
    void findsAllDirectories() {

        List<String> matchingFiles = getMatchingEntries(zipFile,
                isFile().negate());

        assertThat(matchingFiles)
                .containsSequence("1/", "1/1-2/", "2/");
    }
}