package java8.samples.zip;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.codeleak.samples.puzzlers.core.io.ZipFileBrowser;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipFile;

import static org.assertj.core.api.Assertions.assertThat;

class ZipFileBrowserTest {

    private Path path = Paths.get("src/test/resources/zip", "sample-1.zip");
    private ZipFile zipFile;

    @BeforeEach
    void setUp() throws Exception {
        zipFile = new ZipFile(path.toFile());
    }

    @Test
    void findsTextFilesContainingText() {

        List<String> matchingEntries = ZipFileBrowser
                .getMatchingEntries(zipFile, ".*txt", "Line 1");

        assertThat(matchingEntries)
                .containsSequence("1.txt", "1/1-1.txt");
    }

    @Test
    void findsCsvFilesContainingText() {

        List<String> matchingFiles = ZipFileBrowser
                .getMatchingEntries(zipFile, ".*csv", "22");

        assertThat(matchingFiles)
                .containsSequence("1/1-2/1-1-2.csv");
    }

}