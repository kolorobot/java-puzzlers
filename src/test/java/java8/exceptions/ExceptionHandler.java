package java8.exceptions;

import java.util.function.Consumer;

public class ExceptionHandler {

    public static void doInOrderAsync(Runnable first, Runnable second,
                                      Consumer<Throwable> handler) {
        Thread t = new Thread() {
            public void run() {
                try {
                    first.run();
                    second.run();
                } catch (Throwable t) {
                    handler.accept(t);
                }
            }
        };
        t.start();
    }
}
