package java8.exceptions;

import org.junit.jupiter.api.Test;

class ExceptionHandlerTest {

    @Test
    void test() {

        Runnable first = () -> {
            throw new RuntimeException();
        };
        Runnable second = () -> System.out.println("Second...");


        ExceptionHandler.doInOrderAsync(first, second, System.out::println);
    }
}