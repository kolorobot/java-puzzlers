package java8.exceptions;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Supplier;

import static java8.exceptions.Unchecked.unchecked;

class UncheckedTest {

    @Test
    void test() {
        Supplier<String> stringSupplier = unchecked(() -> new String(Files.readAllBytes(
                Paths.get("bad_boys")), StandardCharsets.UTF_8));

        String string = stringSupplier.get();
    }
}