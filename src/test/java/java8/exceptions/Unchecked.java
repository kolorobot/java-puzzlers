package java8.exceptions;

import java.util.function.Supplier;

@FunctionalInterface
interface ExceptionalSupplier<T> {
    T supply() throws Exception;
}

public class Unchecked {

    // we could use Callable here too
    public static <T> Supplier<T> unchecked(ExceptionalSupplier<T> f) {
        return () -> {
            try {
                return f.supply();
            } catch (Exception e) {
                // throw new RuntimeException(e);
            } catch (Throwable t) {
                // throw t;
            }
            return null;
        };
    }
}
