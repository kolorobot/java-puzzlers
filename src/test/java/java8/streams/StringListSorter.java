package java8.streams;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class StringListSorter {

    public List<String> sortByLength(List<String> input) {
        return input.stream()
                .sorted((x, y) -> x.length() - y.length())
                .collect(toList());
    }

    public List<String> sortByLength(List<String> input, boolean reverse) {
        if (!reverse) {
            return sortByLength(input);
        }
        return input.stream()
                .sorted((x, y) -> y.length() - x.length())
                .collect(toList());
    }

    public List<String> sortByFirstCharacter(List<String> input) {
        return input.stream()
                .sorted((x, y) -> y.charAt(0) - x.charAt(0))
                .collect(toList());
    }

    public List<String> sortByContainedCharacter(List<String> input, String letter) {
        return input.stream()
                .sorted((x, y) -> {
                    if (y.contains(letter)) {
                        return 1;
                    } else {
                        return -1;
                    }
                })
                .collect(toList());
    }
}
