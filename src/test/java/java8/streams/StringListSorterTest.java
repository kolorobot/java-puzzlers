package java8.streams;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StringListSorterTest {

    private StringListSorter stringListSorter = new StringListSorter();

    @Test
    void sortsByLength() {
        List<String> result = stringListSorter.sortByLength(
                Arrays.asList("one", "1", "three", "four"));
        assertThat(result).containsSequence("1", "one", "four", "three");
    }

    @Test
    void sortByLengthReversed() {
        List<String> result = stringListSorter.sortByLength(
                Arrays.asList("one", "1", "three", "four"), true);
        assertThat(result).containsSequence("three", "four", "one", "1");
    }

    @Test
    void sortByFirstCharacter() {
        List<String> result = stringListSorter.sortByFirstCharacter(
                Arrays.asList("one", "three", "four", "ten"));
        assertThat(result).containsSequence("three", "ten", "one", "four");
    }

    @Test
    void sortByContainedCharacter() {
        List<String> result = stringListSorter.sortByContainedCharacter(
                Arrays.asList("one", "three", "four", "ten"), "t");
        assertThat(result).containsSequence("three", "ten", "four", "one");
    }
}