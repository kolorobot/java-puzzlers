package java8.patterns.strategy;

@FunctionalInterface
public interface Printer {

    void print(Object value);

    default Printer thenPrinting(Printer other) {
        return value -> {
            this.print(value);
            other.print(value);
        };
    }

    static Printer nullPrinter() {
        return value -> {};
    }

    static Printer consolePrinter() {
        return System.out::println;
    }

    static Printer errorPrinter() {
        return System.err::println;
    }
}
