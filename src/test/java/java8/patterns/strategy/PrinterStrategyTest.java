package java8.patterns.strategy;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class PrinterStrategyTest {

    @Test
    void strategies() {
        List<Printer> printers = Arrays.asList(
                // default strategies
                Printer.nullPrinter(),
                Printer.consolePrinter(),
                Printer.errorPrinter(),
                // Logging strategy
                System.out::println
        );

        Printer.nullPrinter()
                .thenPrinting(Printer.consolePrinter())
                .thenPrinting(Printer.errorPrinter())
                .print("Chained...");

        printers.forEach(printer -> new PrintHouse(printer).print("Printed in Print House ..."));
    }
}