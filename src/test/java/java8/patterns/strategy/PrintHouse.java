package java8.patterns.strategy;

public class PrintHouse {

    private Printer printer; // strategy

    public PrintHouse(Printer printer) {
        this.printer = printer;
    }

    public void print(String value) {
        printer.print(value);
    }
}
