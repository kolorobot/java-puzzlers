package java8.patterns.builder;

import java.util.function.Consumer;
import java.util.stream.Stream;

public class PersonBuilder {

    private final Person person;

    public PersonBuilder(Person person) {
        this.person = person;
    }

    public Person build() {
        return this.person;
    }

    public static PersonBuilder person(PersonSetter... personSetters) {
        Person person = new Person();
        accept(person, personSetters);
        return new PersonBuilder(person);
    }

    public PersonBuilder withAddress(AddressSetter... addressSetters) {
        Address address = new Address();
        accept(address, addressSetters);
        person.setAddress(address);
        return this;
    }

    public static <T> void accept(T obj, Consumer<T>... consumers) {
        Stream.of(consumers).forEach(
                c -> c.accept(obj)
        );
    }
}
