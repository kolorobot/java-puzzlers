package java8.patterns.builder;

import java.util.function.Consumer;

@FunctionalInterface
public interface AddressSetter extends Consumer<Address> {
}
