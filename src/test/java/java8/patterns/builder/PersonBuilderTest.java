package java8.patterns.builder;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PersonBuilderTest {
    @Test
    void test() {
        Person person =
                PersonBuilder.person(
                        p -> p.setId(101101101L),
                        p -> p.setName("John"),
                        p -> p.setEmail("j@email.com")
                ).withAddress(
                        a -> a.setAddressLine1("Address Line 1"),
                        a -> a.setAddressLine2("Address Line 2")
                ).build();


        assertThat(person.getId()).isEqualTo(101101101L);
        assertThat(person.getName()).isEqualTo("John");
        assertThat(person.getEmail()).isEqualTo("j@email.com");
        assertThat(person.getAddress().getAddressLine1()).isEqualTo("Address Line 1");
        assertThat(person.getAddress().getAddressLine2()).isEqualTo("Address Line 2");

    }
}