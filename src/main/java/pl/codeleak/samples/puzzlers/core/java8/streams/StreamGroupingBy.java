package pl.codeleak.samples.puzzlers.core.java8.streams;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamGroupingBy {

    private static class Product {
        BigDecimal price;
        String category;

        BigDecimal getPrice() {
            return price;
        }

        String getCategory() {
            return category;
        }
    }

    public static Map<String, List<Product>> byCategory(List<Product> products) {
        return products.stream()
                       .collect(Collectors.groupingBy(Product::getCategory));
    }
}
