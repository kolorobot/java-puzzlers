package pl.codeleak.samples.puzzlers.core.collections;

import java.util.*;

public class Duplicates {

    public static void main(String[] args) {
        Set<User> users1 = duplicates1();
        System.out.println(users1.size());

        Set<User> users2 = duplicates2();
        System.out.println(users2.size());

        System.out.println(duplicates3().size());
    }

    public static List<Integer> duplicates3() {
        List<Integer> ints = Arrays.asList(1, 2, 3);
        ints.add(1);
        ints.add(2);
        ints.add(3);
        return ints;
    }

    public static Set<User> duplicates2() {
        Set<User> users = Collections.singleton(new User("demo1", "secret1"));
        addUser("demo2", "secret2", users);
        addUser("demo3", "secret3", users);
        addUser("demo4", "secret4", users);

        return users;
    }

    public static Set<User> duplicates1() {
        Set<User> users = new HashSet<>();

        addUser("demo", "secret", users);
        addUser("demo", "secret", users);
        addUser("demo1", "secret1", users);

        return users;
    }

    private static void addUser(String login, String password, Set<User> users) {
        users.add(new User(login, password));
    }
}

class User {

    private final String login;
    private final String password;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof User))
            return false;
        User user = (User) o;
        return Objects.equals(getLogin(), user.getLogin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLogin());
    }
}