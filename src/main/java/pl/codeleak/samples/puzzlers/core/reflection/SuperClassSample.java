package pl.codeleak.samples.puzzlers.core.reflection;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SuperClassSample {
    public static void main(String[] args) throws Exception {
        Collection<Class<?>> sc1 = getSuperClasses(Demo.class.getField("a"));
        System.out.println(sc1);

        Collection<Class<?>> sc2 = getSuperClasses(Demo.class.getField("aa"));
        System.out.println(sc2);

        Collection<Class<?>> sc3 = getSuperClasses(Demo.class.getField("aaa"));
        System.out.println(sc3);
    }

    private static Collection<Class<?>> getSuperClasses(Field field) {
        List<Class<?>> allClasses = new ArrayList<>();
        Class<?> type = field.getType();
        allClasses.add(type);
        Class<?> superclass = type.getSuperclass();
        while (superclass != null) {
            allClasses.add(superclass);
            superclass = superclass.getSuperclass();
        }
        return allClasses;
    }
}

class A {}

class AA extends A {}

class AAA extends AA {}

class Demo {
    public A a;
    public AA aa;
    public AAA aaa;
}
