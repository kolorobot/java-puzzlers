package pl.codeleak.samples.puzzlers.core.java8.exceptions;

import java.util.concurrent.Callable;
import java.util.function.Supplier;

public class Unchecked {

    static <T> Supplier<T> unchecked(Callable<T> callable) {
        return () -> {
            try {
                return callable.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static void main(String[] args) {
        Supplier<String> stringSupplier = unchecked(() -> {
            throw new Exception();
        });

        String string = stringSupplier.get();
    }
}
