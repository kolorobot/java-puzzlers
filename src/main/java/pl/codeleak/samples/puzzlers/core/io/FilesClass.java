package pl.codeleak.samples.puzzlers.core.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class FilesClass {

    static void copy(Path src, Path target) {
        try {
            Instant instant = Files.getLastModifiedTime(src).toInstant();
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());

            DateTimeFormatter yearMonthFormat = DateTimeFormatter.ofPattern("yyyy/MM_(MMM)");
            Path targetDirectory = target.resolve(yearMonthFormat.format(localDateTime));
            Files.createDirectories(targetDirectory);

            Path targetFile = targetDirectory.resolve(src.getFileName());
            if (!Files.exists(targetFile)) {
                Files.copy(src, targetFile);
            }
        } catch (IOException e) {
            // ignore
            System.out.println("Unexpected exception occurred while copying a file ...");
        }
    }

    static void writeRecords(int numOfRecords) throws IOException {
        Path dest = Paths.get("", "");
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(dest, Charset.forName("UTF-8"))) {
            for (int i = 0; i < numOfRecords; i++) {
                bufferedWriter.write("record-" + Double.valueOf(Math.random() * i).intValue() + ";" + i);
                bufferedWriter.newLine();
            }
        }
    }

    public static void main(String[] args) throws IOException {

        Path src = Paths.get("");
        Path target = Paths.get("");

        PathMatcher jpgMatcher = FileSystems.getDefault().getPathMatcher("glob:**.{jpg}");
        Files.find(src, Integer.MAX_VALUE,
            (path, attributes) -> jpgMatcher.matches(path))
             .parallel()
             .forEach(p -> copy(p, target));
    }
}
