package pl.codeleak.samples.puzzlers.core.java8.streams;

import java.util.List;
import java.util.stream.Collectors;

public class StreamJoining {
    private static class Product {

        String name;

        String getName() {
            return name;
        }
    }

    public static String names(List<Product> products) {
        return products.stream()
                       .map(Product::getName)
                       .collect(Collectors.joining(","));
    }
}
