package pl.codeleak.samples.puzzlers.core.exceptions;

public class TryCatchFinally {

    public static void main(String[] args) {
        System.out.println(whatIsTheResult());
        doIEverGetPrinted();
    }

    private static boolean whatIsTheResult() {
        try {
            return true;
        } finally {
            System.out.println("???");
//            return false;
        }
    }

    private static void doIEverGetPrinted() {
        try {
            throw new NullPointerException("NullPointerException (1)");
        } catch (NullPointerException e) {
            throw new NullPointerException("NullPointerException (2)");
        } finally {
            System.out.println("!!!");
        }
    }
}
