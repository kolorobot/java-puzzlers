package pl.codeleak.samples.puzzlers.core.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipFileBrowser {

    public static List<String> getMatchingEntries(ZipFile zipFile,
                                                  String pattern,
                                                  String contains) {

        Predicate<ZipEntry> isFile = ze -> !ze.isDirectory();
        Predicate<ZipEntry> matchesPattern = ze -> ze.getName().matches(pattern);

        return zipFile.stream()
                      .filter(isFile.and(matchesPattern))
                      .filter(ze -> containsText(zipFile, ze, contains))
                      .map(ZipEntry::toString)
                      .collect(Collectors.toList());
    }

    private static boolean containsText(ZipFile zipFile, ZipEntry zipEntry, String contains) {

        if (zipEntry.isDirectory()) {
            return false;
        }

        try (InputStream inputStream = zipFile.getInputStream(zipEntry);
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            Optional<String> found = reader.lines()
                                           .filter(l -> l.contains(contains))
                                           .findFirst();
            return found.isPresent();
        } catch (IOException e) {
            return false;
        }
    }
}
