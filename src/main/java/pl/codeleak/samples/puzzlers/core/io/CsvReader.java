package pl.codeleak.samples.puzzlers.core.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.stream.Collectors;

interface RowMapper<IN, OUT> {
    OUT map(IN input);
}

class Code {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

class CsvReader {

    private static final String SEPARATOR = ";";

    private final Reader source;
    private final String separator;
    private final RowMapper<String[], Code> rowMapper;

    CsvReader(Reader source, RowMapper<String[], Code> rowMapper) {
        this(source, SEPARATOR, rowMapper);
    }

    CsvReader(Reader source, String separator, RowMapper<String[], Code> rowMapper) {
        this.source = source;
        this.separator = separator;
        this.rowMapper = rowMapper;
    }

    List<Code> getAll() {
        try (BufferedReader reader = new BufferedReader(source)) {
            return reader.lines()
                         .map(line -> line.split(separator))
                         .map(rowMapper::map)
                         .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
