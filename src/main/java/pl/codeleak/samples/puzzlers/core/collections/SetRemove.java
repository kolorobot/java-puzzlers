package pl.codeleak.samples.puzzlers.core.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

public class SetRemove {

    static Logger logger = Logger.getLogger("Iterate");

    static void removeWhileIterating1(Set<String> names) {
        Iterator<String> iterator = names.iterator();
        while (iterator.hasNext()) {
                if (iterator.next().equals("Mary")) {
                    iterator.remove();
                }
        }
        logger.info(names.toString());
    }

    static void removeWhileIterating2(Set<String> names) {
        for (Iterator<String> iterator = names.iterator(); iterator.hasNext(); ) {
            if (iterator.next().equals("Mary")) {
                iterator.remove();
            }
        }
        logger.info(names.toString());
    }


    static void removeWhileIterating3(Set<String> names) {
        names.stream()
             .forEach(name -> {
                 if (name.equals("Mary")) {
                     names.remove(name);
                 }
             });
        logger.info(names.toString());
    }

    static void removeWhileIterating4(Set<String> names) {
        for (String name : names) {
            if (name.equals("Mary")) {
                names.remove(name);
            }
        }
        logger.info(names.toString());
    }

    static void removeJava8(Set<String> names) {
        names.removeIf(name -> name.equals("Mary"));
        logger.info(names.toString());
    }


    public static void main(String[] args) {
        Set<String> names = new HashSet<>();
        names.add("John");
        names.add("Scott");
        names.add("Mary");
        names.add("Andy");
        names.add("Mary");

        invoke("removeWhileIterating1", () -> removeWhileIterating1(new HashSet<>(names)));
        invoke("removeWhileIterating2", () -> removeWhileIterating2(new HashSet<>(names)));
        invoke("removeWhileIterating3", () -> removeWhileIterating3(new HashSet<>(names)));
        invoke("removeWhileIterating4", () -> removeWhileIterating4(new HashSet<>(names)));
        invoke("removeJava8", () -> removeJava8(new HashSet<>(names)));


    }

    private static void invoke(String name, Runnable runnable) {
        try {
            logger.info("Invoke " + name);
            runnable.run();
        } catch (Exception e) {
            logger.warning(name + " - " + e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
