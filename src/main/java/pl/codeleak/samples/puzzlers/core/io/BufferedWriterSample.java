package pl.codeleak.samples.puzzlers.core.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BufferedWriterSample {
    public static void main(String[] args) throws IOException {
        Path dest = Paths.get("", "");
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(dest, Charset.forName("UTF-8"))) {
            for (int i = 0; i < 1000; i++) {
                bufferedWriter.write("record-" + Double.valueOf(Math.random() * i).intValue() + ";" + i);
                bufferedWriter.newLine();
            }
        }
    }
}
