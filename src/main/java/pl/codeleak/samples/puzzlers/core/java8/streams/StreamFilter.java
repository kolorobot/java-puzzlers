package pl.codeleak.samples.puzzlers.core.java8.streams;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamFilter {

    public static List<String> lowerThan(List<String> collection, int length) {
        return filter(collection, s -> s.length() < length);
    }

    public static List<String> filter(List<String> collection, Predicate<String> predicate) {
        return collection.stream()
                         .filter(predicate)
                         .collect(Collectors.toList());
    }
}
