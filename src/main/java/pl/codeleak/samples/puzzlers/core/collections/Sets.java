package pl.codeleak.samples.puzzlers.core.collections;

import java.util.LinkedHashSet;
import java.util.Set;

public class Sets {

    static class A {
        private final int num;

        A(int num) {
            this.num = num;
        }

        @Override
        public String toString() {
            return num + "";
        }
    }

    public static void set1() {
        Set<A> set = new LinkedHashSet<>();
        set.add(new A(1));
        set.add(new A(3));
        set.add(new A(4));
        set.add(new A(2));

        System.out.println(set);
    }

    public static void main(String[] args) {
        set1();
    }

}
