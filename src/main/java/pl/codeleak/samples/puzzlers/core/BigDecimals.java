package pl.codeleak.samples.puzzlers.core;

import java.math.BigDecimal;

public class BigDecimals {

    public static void main(String[] args) {
        BigDecimal twoZero = new BigDecimal(2.0);
        BigDecimal twoZeroZero = new BigDecimal(2.00);

        System.out.println(twoZero.equals(twoZeroZero)); // true
        System.out.println(twoZero.compareTo(twoZeroZero));

        BigDecimal anotherTwoZero = new     BigDecimal("2.0");
        BigDecimal anotherTwoZeroZero = new BigDecimal("2.00");

        System.out.       println(anotherTwoZero.equals(anotherTwoZeroZero)); // false
        System.out.println(anotherTwoZero.compareTo(anotherTwoZeroZero));
    }
}
