package pl.codeleak.samples.puzzlers.core;

import java.util.Set;

public class Immutable {

    private final String name;
    private final Set<Integer> ints;

    public Immutable(String name, Set<Integer> ints) {
        this.name = name;
        this.ints = ints;
    }

    public String getName() {
        return name;
    }

    public Set<Integer> getInts() {
        return ints;
    }
}
