package pl.codeleak.samples.puzzlers.core;

public class PassingByValue {

    public static void main(String[] args) {

        int i = 5;
        Integer obj = Integer.valueOf("10");

        changePrimitive(i);
        changeObject(obj);

        System.out.println(i);
        System.out.println(obj);
    }

    static void changePrimitive(int i) {
        i = 20;
    }

    static void changeObject(Integer j) {
        j = Integer.valueOf("30");
    }
}

