package pl.codeleak.samples.puzzlers.core.io;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class VCardReader {

    private final Reader source;
    private static final String ITEM1_TAG = "item1.";
    private static final String END_TAG = "END:VCARD";
    private static final String PHONE_TAG = "TEL;TYPE";

    public VCardReader(Reader source) {
        this.source = source;
    }

    public List<List<String>> readAll() {

        List<List<String>> result = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(source)) {
            List<String> lines = br.lines().collect(Collectors.toList());
            List<String> record = new ArrayList<>();

            lines.forEach(c -> {
                if (!c.startsWith(ITEM1_TAG)) {
                    record.add(c);
                }
                if (c.startsWith(END_TAG)) {
                    result.add(new ArrayList<>(record));
                    record.clear();
                }
            });

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return result;
    }

    public void saveAll(Path path, List<List<String>> records) {
        List<String> flattenedList = records.stream().flatMap(strings -> strings.stream()).collect(Collectors.toList());
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path.toFile()))) {
            for (String l : flattenedList) {
                bw.append(l);
                bw.newLine();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        VCardReader vCardReader = new VCardReader(new FileReader("contacts.vcf"));
        List<List<String>> allRecords = vCardReader.readAll();
        List<List<String>> withPhone = allRecords.stream().filter(hasPhone()).collect(Collectors.toList());

        System.out.println(allRecords);
        System.out.println(withPhone);

        vCardReader.saveAll(Paths.get("contacts_new.vcf"), withPhone);
    }

    private static Predicate<List<String>> hasPhone() {
        return record -> record.stream().filter(line -> line.startsWith(PHONE_TAG)).findFirst().isPresent();
    }

}
