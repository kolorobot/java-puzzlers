package pl.codeleak.samples.puzzlers.core.exceptions;

public class TryWithResources {
    public static void main(String[] args) {
        try (A a = new A()) {
            a.doSomething();
        }

        try (A b = new B()) {
            b.doSomething();
        }
    }
}

class A implements AutoCloseable {

    void doSomething() {
        System.out.println("I am doing something!");
    }

    @Override
    public void close() {
        System.out.println("Closing..");
    }
}

class B extends A {

    @Override
    public void close() {
        throw new RuntimeException("Cannot close...");
    }
}

