package pl.codeleak.samples.puzzlers.core.collections;

import java.util.LinkedList;
import java.util.Queue;

public class Queues {

    static class A {
        private final int num;
        A(int num) {
            this.num = num;
        }

        @Override
        public String toString() {
            return num + "";
        }
    }

    public static void main(String[] args) {
        Queue queue = new LinkedList();

        queue.offer(new A(1));
        queue.offer(new A(2));
        queue.offer(new A(3));

        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());

    }
}
