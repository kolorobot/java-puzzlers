package pl.codeleak.samples.puzzlers.core.java8.streams;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamFlatMap {

    public List<String> flatten(Map<String, List<String>> map) {
        return map.values().stream()
                  .flatMap(Collection::stream)
                  .collect(Collectors.toList());
    }
}
