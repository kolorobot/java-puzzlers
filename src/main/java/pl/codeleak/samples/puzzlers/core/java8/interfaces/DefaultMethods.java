package pl.codeleak.samples.puzzlers.core.java8.interfaces;

public class DefaultMethods {
    public static void main(String[] args) {
        Printer printer = System.out::println;
        printer.thenPrinting(System.err::println).print("Chained printing ...");
    }
}

@FunctionalInterface
interface Printer {

    void print(String value);

    default Printer thenPrinting(Printer other) {
        return value -> {
            this.print(value);
            other.print(value);
        };
    }

    static Printer nullPrinter() {
        return value -> {
        };
    }
}
