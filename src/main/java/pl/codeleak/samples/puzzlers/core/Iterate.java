package pl.codeleak.samples.puzzlers.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class Iterate {

    static Logger logger = Logger.getLogger("Iterate");

    static void iterate1(List<String> names) {
        Iterator<String> iterator = names.iterator();
        while (iterator.hasNext()) {
            logger.info(iterator.next());
        }
    }

    static void iterate2(List<String> names) {
        for (String name : names) {
            logger.info(name);
        }
    }

    static void iterate3(List<String> names) {
        names.stream()
             .forEach(name -> logger.info(name));
    }

    static void iterate4(List<String> names) {
        names.stream()
             .peek(name -> logger.info(name));
    }

    public static void main(String[] args) {

        List<String> names = new ArrayList<>();
        names.add("John");
        names.add("Scott");
        names.add("Mary");
        names.add("Andy");
        names.add("Mary");

        iterate1(new ArrayList<>(names));
        iterate2(new ArrayList<>(names));
        iterate3(new ArrayList<>(names));
        iterate4(new ArrayList<>(names));
    }
}