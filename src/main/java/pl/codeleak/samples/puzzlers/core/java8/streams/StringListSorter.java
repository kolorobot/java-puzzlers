package pl.codeleak.samples.puzzlers.core.java8.streams;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class StringListSorter {

    List<String> sortByLength(List<String> input) {
        return input.stream()
                    .sorted((x, y) -> x.length() - y.length())
                    .collect(toList());
    }

    List<String> sortByLength(List<String> input, boolean reverse) {
        if (!reverse) {
            return sortByLength(input);
        }
        return input.stream()
                    .sorted((x, y) -> y.length() - x.length())
                    .collect(toList());
    }

    List<String> sortByFirstCharacter(List<String> input) {
        return input.stream()
                    .sorted((x, y) -> y.charAt(0) - x.charAt(0))
                    .collect(toList());
    }

    List<String> sortByContainedCharacter(List<String> input, String letter) {
        return input.stream()
                    .sorted((x, y) -> {
                        if (y.contains(letter)) {
                            return 1;
                        } else {
                            return -1;
                        }
                    })
                    .collect(toList());
    }

    public static void main(String[] args) {
        StringListSorter stringListSorter = new StringListSorter();

        System.out.println(stringListSorter.sortByLength(
            Arrays.asList("one", "1", "three", "four")));

        System.out.println(stringListSorter.sortByLength(
            Arrays.asList("one", "1", "three", "four"), true));

        System.out.println(stringListSorter.sortByFirstCharacter(
            Arrays.asList("one", "three", "four", "ten")));

        System.out.println(stringListSorter.sortByContainedCharacter(
            Arrays.asList("one", "three", "four", "ten"), "t"));
    }
}
