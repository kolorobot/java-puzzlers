package pl.codeleak.samples.puzzlers.core;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SalaryCalculation {

    private static class Employee {

        String name;

        BigDecimal salary;

        public Employee(String name, BigDecimal salary) {
            this.name = name;
            this.salary = salary;
        }

        BigDecimal getSalary() {
            return salary;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static Employee deservesSalaryRise(List<Employee> employees) {
        return employees.stream()
                        .collect(Collectors.minBy(Comparator.comparing(Employee::getSalary))).get();
    }

    public static void main(String[] args) {
        Employee e1 = new Employee("E1", BigDecimal.valueOf(280000, 2));
        Employee e2 = new Employee("E2", BigDecimal.valueOf(190000, 2));
        Employee e3 = new Employee("E3", BigDecimal.valueOf(210000, 2));

        System.out.println(deservesSalaryRise(Arrays.asList(e1, e2, e3)));
    }
}
