package pl.codeleak.samples.puzzlers.core;

import java.math.BigDecimal;

public class Comparing {

    private static class Product implements Comparable<Product> {
        BigDecimal price;

        BigDecimal getPrice() {
            return price;
        }

        @Override
        public int compareTo(Product o) {
            return getPrice().compareTo(o.getPrice());
        }
    }

    public static void main(String[] args) {
        Product p1 = new Product();
        p1.price = BigDecimal.valueOf(10065, 2);

        Product p2 = new Product();
        p2.price = BigDecimal.valueOf(10066, 2);

        System.out.println(p1.getPrice());
        System.out.println(p2.getPrice());

        System.out.println(p1.compareTo(p2)); // p1 < p2
        System.out.println(p2.compareTo(p1)); // p2 > p1

    }


}
