package pl.codeleak.samples.puzzlers.core.collections;

import java.util.TreeMap;

public class TreeMaps {

    private static TreeMap<Double, Double> boundedIntervals;

    static {
        boundedIntervals = new TreeMap<>();
        boundedIntervals.put(0.0, 1.0);
        boundedIntervals.put(12.0, 0.95);
        boundedIntervals.put(16.0, 0.9);
        boundedIntervals.put(21.0, 0.85);
        boundedIntervals.put(26.0, 0.8);
        boundedIntervals.put(31.0, 0.75);
    }

    public static void main(String[] args) {
        Double referenceValue = 12.0;
        System.out.println(boundedIntervals.floorEntry(referenceValue).getValue());
        System.out.println(boundedIntervals.floorKey(referenceValue));

        System.out.println(boundedIntervals.ceilingEntry(referenceValue).getValue());
        System.out.println(boundedIntervals.ceilingKey(referenceValue));

        System.out.println(boundedIntervals.lastEntry().getValue());
        System.out.println(boundedIntervals.firstEntry().getValue());

        System.out.println(boundedIntervals.descendingMap());
    }
}
