package pl.codeleak.samples.puzzlers.core.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Converter {

    public static void main(String[] args) {
        List<OutputDto> outputDtos = convert(Arrays.asList(new Input(), new Input(), new Input()));

    }

    public static List<OutputDto> convert(List<Input> inputs) {
        List<OutputDto> dtos = new ArrayList<OutputDto>();
        for (Input input : inputs) {
            dtos.add(convertToDto(input));
        }
        return dtos;
    }

    private static OutputDto convertToDto(Input input) {
        return new OutputDto();
    }

    private static class Input {}

    private static class OutputDto {}
}
