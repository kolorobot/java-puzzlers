package pl.codeleak.samples.puzzlers.core;

public class Interfaces {

}


interface Int1 {

}

abstract interface Int2 {

}

interface Int3 {
    int a = 1;

    // int b; compile error - must be initialized

    public static final String A = "C";
}



