package pl.codeleak.samples.puzzlers.core.java8.streams;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamMap {

    public List<String> toUpperCase(List<String> collection) {
        return transform(collection, String::toUpperCase);
    }

    public List<String> toLowerCase(List<String> collection) {
        return transform(collection, String::toUpperCase);
    }

    public List<String> transform(List<String> collection, Function<String, String> function) {
        return collection.stream()
                         .map(function)
                         .collect(Collectors.toList());
    }
}
