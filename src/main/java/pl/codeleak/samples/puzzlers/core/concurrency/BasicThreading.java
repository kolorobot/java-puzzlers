package pl.codeleak.samples.puzzlers.core.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class BasicThreading {

    static void runnable() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.println("I am runnable ... 1");
            }
        };

        Thread thread = new Thread(r);
        thread.start();
    }

    static void runnableJava8() {
        Runnable r = () -> System.out.println("I am runnable ... 2");
        Thread thread = new Thread(r);
        thread.start();
    }

    static void threadExecutor() {
        ExecutorService e = Executors.newSingleThreadExecutor();
        e.execute(() -> System.out.println("I am runnable ... 3"));
        e.shutdown();
    }

    static void threadFactory() {
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        Thread thread = threadFactory.newThread(() -> System.out.println("I am runnable ... 4"));
        thread.start();
    }

    static void callable() throws Exception {
        Callable<String> callable = Executors.callable(() -> System.out.println("I am runnable ... 5"), "Done");
        ExecutorService pool = Executors.newCachedThreadPool();
        Future<String> future = pool.submit(callable);
        while (!future.isDone()) {
            System.out.println(future.get(1, TimeUnit.SECONDS));
        }
        pool.shutdown();
    }

    static void whatIsTheOrderOfExecution() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        IntStream.range(1, 11)
                .forEach(i -> executorService.execute(createRunnable(i)));
        executorService.shutdown();
    }

    static void whatIsTheOrder() throws InterruptedException {
        List<Callable<String>> callables = new ArrayList<>(10);
        IntStream.range(1, 11)
                .forEach(i -> callables.add(Executors.callable(createRunnable(i), "Result of #" + i)));

        ExecutorService executorService = Executors.newCachedThreadPool();
        List<Future<String>> futures = executorService.invokeAll(callables);

        futures.forEach(future -> {
            try {
                System.out.println(future.get());
            } catch (InterruptedException e) {
            } catch (ExecutionException e) {
            }
        });

        executorService.shutdown();
    }

    private static Runnable createRunnable(int i) {
        return () -> {
            try {
                Thread.sleep(new Random().nextInt(1000));
                System.out.println("I am runnable ... " + i);
            } catch (InterruptedException e) {
            }
        };
    }

    public static void main(String[] args) throws Exception {
//        runnable();
//        runnableJava8();
//        threadExecutor();
//        threadFactory();
//        callable();
//        whatIsTheOrderOfExecution();
        whatIsTheOrder();
    }

}
