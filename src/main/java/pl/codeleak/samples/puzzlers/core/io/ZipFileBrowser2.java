package pl.codeleak.samples.puzzlers.core.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipFileBrowser2 {

    public static List<String> getMatchingEntries(ZipFile zipFile,
                                                  Predicate<ZipEntry> entryFilters) {
        return zipFile.stream()
                .filter(entryFilters)
                .map(ZipEntry::toString)
                .collect(Collectors.toList());
    }

    public static List<String> getMatchingEntries(ZipFile zipFile,
                                                  Predicate<ZipEntry> entryFilters,
                                                  Predicate<String> contentFilters) {
        return zipFile.stream()
                .filter(entryFilters)
                .filter(zipEntry -> contentFilter(zipFile, zipEntry, contentFilters))
                .map(ZipEntry::toString)
                .collect(Collectors.toList());
    }

    public static boolean contentFilter(ZipFile zipFile, ZipEntry zipEntry, Predicate<String> contentFilters) {
        if (zipEntry.isDirectory()) {
            return false;
        }

        try (InputStream inputStream = zipFile.getInputStream(zipEntry);
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            return reader.lines().filter(contentFilters).findFirst().isPresent();
        } catch (IOException e) {
            return false;
        }
    }

    public static Predicate<ZipEntry> isFile() {
        return ze -> !ze.isDirectory();
    }

    public static Predicate<ZipEntry> nameMatches(String pattern) {
        return ze -> ze.getName().matches(pattern);
    }

    public static Predicate<ZipEntry> nameContains(String sequence) {
        return ze -> ze.getName().contains(sequence);
    }

    public static Predicate<String> withContent(String substring) {
        return s -> s.contains(substring);
    }
}
