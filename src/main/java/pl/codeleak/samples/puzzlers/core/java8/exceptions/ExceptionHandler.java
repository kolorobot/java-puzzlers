package pl.codeleak.samples.puzzlers.core.java8.exceptions;

import java.util.function.Consumer;

class ExceptionHandler {

    static void run(Runnable task, Consumer<Throwable> handler) {
        Thread t = new Thread() {
            public void run() {
                try {
                    task.run();
                } catch (Throwable t) {
                    handler.accept(t);
                }
            }
        };
        t.start();
    }

    public static void main(String[] args) {
        Runnable task = () -> {
            throw new RuntimeException();
        };
        ExceptionHandler.run(task, System.out::println);
    }
}
