package pl.codeleak.samples.puzzlers.core;

public class Strings {

    public static void main(String[] args) {
        Strings strings = new Strings();
        strings.concat1();
        strings.concat2();
        strings.equality1();
        strings.equality2();
        strings.equality3();
    }

    void concat1() {
        String myString = "myString";
        myString.concat(" got changed");

        System.out.println(myString);
    }

    void concat2() {
        String myString = 42 + "myString";

        System.out.println(myString);
    }

    void equality1() {
        String myString = "myString";
        String myString1 = "myString";
        String myString2 = myString1;

        System.out.println(myString == myString1);
        System.out.println(myString1 == myString2);
    }

    void equality2() {
        String myString = new String("myString");
        String myString1 = new String("myString");
        String myString2 = "myString";

        System.out.println(myString == myString1);
        System.out.println(myString1 == myString2);
    }

    void equality3() {
        String myString = new StringBuilder("myString").toString();
        String myString1 = new StringBuilder("myString").toString();

        System.out.println(myString == myString1);
    }

}
