package pl.codeleak.samples.puzzlers.core.exceptions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

public class TryCatch {

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader("no_file"))) {
            String firstLine = readFirstLine(reader);
        } catch (FileNotFoundException e) {
            // handle no file
        } catch (NoFirstLine e) {
            // handle no first line
        } catch (IOException e) {
            // handle general exception
        }
    }

    private static String readFirstLine(BufferedReader reader) throws NoFirstLine {
        Optional<String> first = reader.lines().findFirst();
        return first.orElseThrow(NoFirstLine::new);
    }
}

class NoFirstLine extends IOException {

}

