package pl.codeleak.samples.puzzlers.core.java8.interfaces;

public class DefaultMethods2 {
    public static void main(String[] args) {
        I i = () -> { /* do nothing */ };
        i.myNameIs();

        J j = () -> { /* do nothing */ };
        j.myNameIs();

        K k = () -> { /* do nothing */ };
        k.myNameIs();
    }
}

@FunctionalInterface
interface I {

    public final  String NAME = "I am I";

    void doSomething();

    default void myNameIs() {
        System.out.println(NAME);
    }
}

interface J extends I {
    @Override
    default void myNameIs() {
        System.out.println(".. and I am J");
    }
}

interface K extends I {

}
