package pl.codeleak.samples.puzzlers.core.java8.streams;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

public class StreamMaxMinSumProductPrice {

    private static class Product {
        BigDecimal price;

        BigDecimal getPrice() {
            return price;
        }
    }

    public static Product minPrice(List<Product> products) {
        return products.stream()
                       .min(Comparator.comparing(Product::getPrice))
                       .get();
    }

    public static Product maxPrice(List<Product> products) {
        return products.stream()
                       .max(Comparator.comparing(Product::getPrice))
                       .get();
    }

    public static BigDecimal sumPrice(List<Product> products) {
        return products.stream()
                       .map(Product::getPrice)
                       .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
