package pl.codeleak.samples.puzzlers.core.java8;

import java.util.Comparator;
import java.util.function.Predicate;

public class LambdaExpressions {

    public void noArgumentsNoBody() throws InterruptedException {
        Runnable r = () -> simpleTask();
        Thread t1 = new Thread(r);
        t1.start();
        t1.join();
    }

    public void withArguments() {
        Predicate<String> p1 = s -> s.length() > 0;
        Predicate<String> p2 = (String s) -> s.length() > 0;

        Comparator<String> c = (x, y) -> x.compareTo(y);
    }

    public void theBlockForm() throws InterruptedException {
        Predicate<String> p = (String s) -> {
            System.out.println("p");
            return s.length() > 0;
        };
    }

    public void methodReferences() throws InterruptedException {
        Runnable r1 = LambdaExpressions::staticSimpleTask;
        Runnable r2 = this::simpleTask;

        Thread t1 = new Thread(r1);
        t1.start();
        Thread t2 = new Thread(r2);
        t2.start();

        t1.join();
        t2.join();
    }

    private void simpleTask() {
        System.out.println("My first lambda expression!");
    }

    private static void staticSimpleTask() {
        System.out.println("Hello!");
    }
}
