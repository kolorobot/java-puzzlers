package pl.codeleak.samples.puzzlers.core.java8.interfaces;

public class StaticMethods {
    public static void main(String[] args) {
        Holder<Integer> holder = Holder.of(Integer.valueOf(100));
        System.out.println(holder.getValue());
    }
}

@FunctionalInterface
interface Holder<T> {

    T getValue();

    static <T> Holder of(T value) {
        return () -> value;
    }
}