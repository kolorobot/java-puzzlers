package pl.codeleak.samples.puzzlers.patterns;

public class FluentBuilder {
    public static void main(String[] args) {
        MutableUser user = new UserBuilder()
            .withLogin("demo")
            .withPassword("secret")
            .build();
    }
}

class UserBuilder {

    private MutableUser user = new MutableUser();

    public UserBuilder withLogin(String login) {
        user.setLogin(login);
        return this;
    }

    public UserBuilder withPassword(String password) {
        user.setPassword(password);
        return this;
    }

    public MutableUser build() {
        return user;
    }
}

class MutableUser {

    private String login;
    private String password;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}