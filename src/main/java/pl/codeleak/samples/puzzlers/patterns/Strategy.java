package pl.codeleak.samples.puzzlers.patterns;

import java.util.Arrays;
import java.util.List;

public class Strategy {

    public static void main(String[] args) {
        List<Printer> printers = Arrays.asList(
            // default strategies
            Printer.nullPrinter(),
            Printer.consolePrinter(),
            Printer.errorPrinter(),
            // ad-hoc strategy
            value -> System.out.println(String.valueOf(value))
        );
        printers.forEach(printer -> new PrintHouse(printer).print("Printed in Print House ..."));
    }
}

class PrintHouse {

    private Printer printer; // strategy

    public PrintHouse(Printer printer) {
        this.printer = printer;
    }

    public void print(String value) {
        printer.print(value);
    }
}


@FunctionalInterface
interface Printer {

    void print(Object value);

    default Printer thenPrinting(Printer other) {
        return value -> {
            this.print(value);
            other.print(value);
        };
    }

    static Printer nullPrinter() {
        return value -> {
        };
    }

    static Printer consolePrinter() {
        return System.out::println;
    }

    static Printer errorPrinter() {
        return System.err::println;
    }
}