package pl.codeleak.samples.puzzlers.solid;

public class Liskov {

    public static void main(String[] args) {
        Animal animal = new Programmer();
        makeSomeNoise(animal);
    }

    static void makeSomeNoise(Animal animal) {
        System.out.println(animal.makeNoise());
    }
}

interface Animal {
    String makeNoise();
}

class Dog implements Animal {

    @Override
    public String makeNoise() {
        return "Wouf";
    }
}

class Cat implements Animal {

    @Override
    public String makeNoise() {
        return "Miau";
    }
}

class Programmer implements Animal {

    @Override
    public String makeNoise() {
        throw new UnsupportedOperationException();
    }
}