package pl.codeleak.samples.puzzlers.oop;

public class Shapes {

    public static void main(String[] args) {
        Shape s1 = new Shape();

        Shape s2 = new Circle();

        s1.draw();
        s2.draw();
        s2.draw("Cricle");
    }
}

class Shape {

    Shape() {
        System.out.println("Shape");
    }

    void draw() {
        System.out.println("Shape Drawing");
    }

    void draw(String what) {
        System.out.println("Shape Drawing " + what);
    }
}

class Circle extends Shape {

    public Circle() {
        System.out.println("Cricle");
    }

    @Override
    void draw() {
        System.out.println("Circle drawing");
    }
}